package com.example.p52.di

import android.content.Context
import androidx.room.Room
import com.example.p52.room.UserDao
import com.example.p52.room.UserDataBase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
class DaoModule {
    @Singleton
    @Provides
    fun provideDao(@ApplicationContext context:Context): UserDao {
        return Room.databaseBuilder(
            context,
            UserDataBase::class.java, "UserDataBase"
        ).build().getDao()
    }
}
