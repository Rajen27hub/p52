package com.example.p52.di

import com.example.p52.BASE_URL
import com.example.p52.api.ApiService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton
import retrofit2.Retrofit
import retrofit2.adapter.rxjava3.RxJava3CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Named

@InstallIn(SingletonComponent::class)
@Module
class RestModule {

    @Singleton
    @Provides
    fun provideRetrofit(): Retrofit =
        Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addCallAdapterFactory(RxJava3CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .build()

    @Named("user")
    @Singleton
    @Provides
    fun provideService(retrofit: Retrofit): ApiService {
        return retrofit.create(ApiService::class.java)
    }

    @Named("album")
    @Singleton
    @Provides
    fun provideAlbumService(retrofit: Retrofit): ApiService {
        return retrofit.create(ApiService::class.java)
    }


    @Named("photo")
    @Singleton
    @Provides
    fun providePhotoService(retrofit: Retrofit): ApiService {
        return retrofit.create(ApiService::class.java)
    }
}
