package com.example.p52.api

import com.example.p52.model.Albums
import com.example.p52.model.Photos
import com.example.p52.model.Users
import io.reactivex.rxjava3.core.Observable
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {
    @GET("users")
    fun getUsers(): Observable<List<Users>>

    @GET("albums")
    fun getAlbums(@Query("userId") id:Int): Observable<List<Albums>>

    @GET("photos")
    fun getPhotos(@Query("albumId") id: Int): Observable<List<Photos>>
}