package com.example.p52

const val HOME_ROUTE = "MainView"
const val BASE_URL = "http://jsonplaceholder.typicode.com/"

sealed class UserNavigation(val route: String) {
    object AlbumView : UserNavigation("AlbumView")
}