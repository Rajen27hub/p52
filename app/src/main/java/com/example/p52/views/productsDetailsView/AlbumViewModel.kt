package com.example.p52.views.productsDetailsView

import android.util.Log
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.p52.api.ApiService
import com.example.p52.model.Albums
import com.example.p52.model.Users
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.schedulers.Schedulers
import javax.inject.Inject
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import javax.inject.Named

@HiltViewModel
class AlbumViewModel @Inject constructor(
    @Named("album") private val service: ApiService
) : ViewModel() {
    val state = MutableStateFlow<ProductDetailsState>(ProductDetailsState.START)
    var albumListResponse = MutableStateFlow<List<Albums>>(emptyList())
    var errorMessage: String by mutableStateOf("")
    private val disposable = CompositeDisposable()

    private fun getAlbumList(i: Int) = viewModelScope.launch {
        state.value = ProductDetailsState.LOADING
        disposable.add(
            service.getAlbums(i)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    state.value = ProductDetailsState.SUCCESS(
                        album = it
                    )
                }, { e ->
                    errorMessage = e.message.toString()
                    Log.i("album", errorMessage)
                })
        )
    }

    fun onStart(id: Int) {
        getAlbumList(i = id)
    }

    fun resetState() {
        state.value = ProductDetailsState.START
    }
}

sealed class ProductDetailsState {
    object START : ProductDetailsState()
    object LOADING : ProductDetailsState()
    data class SUCCESS(
        val album: List<Albums>
    ) : ProductDetailsState()

    data class FAILURE(val failureMessage: String) : ProductDetailsState()
}