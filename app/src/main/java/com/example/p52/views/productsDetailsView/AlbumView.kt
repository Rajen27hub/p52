package com.example.p52.views.productsDetailsView

import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import com.example.p52.R
import com.example.p52.alerter.showBanner
import com.example.p52.model.Albums


@Composable
fun AlbumView(id: Int, navController: NavController) {
    val viewModel = hiltViewModel<AlbumViewModel>()
    val context = LocalContext.current
    LaunchedEffect(key1 = Unit, block = {
        viewModel.onStart(id)
    })
    val state by viewModel.state.collectAsState()
    state.let {
        when (it) {
            ProductDetailsState.LOADING -> {
                Box(
                    contentAlignment = Alignment.Center,
                    modifier = Modifier.fillMaxSize()
                ) {
                    CircularProgressIndicator(color = colorResource(id = R.color.purple_200))
                }
            }
            is ProductDetailsState.SUCCESS -> {
                val album = it.album
                LazyColumn(contentPadding = PaddingValues(16.dp)) {
                    itemsIndexed(album) { _, item ->
                        AlbumCardView(album = item, navController = navController)
                    }
                }
            }
            is ProductDetailsState.FAILURE -> {
                val message = it.failureMessage
                LaunchedEffect(key1 = message) {
                    showBanner(context, message)
                    viewModel.resetState()
                }
            }
            else -> {}
        }
    }
}

@Composable
fun AlbumCardView(album: Albums, navController: NavController) {
    Column(
        modifier = Modifier
            .fillMaxSize()
            .padding(12.dp),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Top
    ) {
        Text(
            text = album.title,
            style = MaterialTheme.typography.subtitle1,
            textAlign = TextAlign.Center,
            modifier = Modifier.padding(4.dp),
            fontWeight = FontWeight.Bold
        )
    }
}
