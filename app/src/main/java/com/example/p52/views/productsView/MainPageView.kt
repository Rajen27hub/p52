package com.example.p52.views.productsView

import android.annotation.SuppressLint
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.PaddingValues
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.itemsIndexed
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavController
import com.example.p52.R
import com.example.p52.UserNavigation
import com.example.p52.alerter.showBanner
import com.example.p52.model.Users

@Composable
fun MainPageView(navController: NavController) {
    val viewModel = hiltViewModel<MainPageViewModel>()
    val context = LocalContext.current
    LaunchedEffect(key1 = Unit, block = {
        viewModel.onStart()
    })
    val state by viewModel.state.collectAsState()
    state.let {
        when (it) {
            ProductState.LOADING -> {
                Box(
                    contentAlignment = Alignment.Center,
                    modifier = Modifier.fillMaxSize()
                ) {
                    CircularProgressIndicator(color = colorResource(id = R.color.purple_200))
                }
            }
            is ProductState.SUCCESS -> {
                val users = it.userList
                LazyColumn(contentPadding = PaddingValues(16.dp)) {
                    itemsIndexed(users) { _, item ->
                        UserCardView(user = item, navController = navController)
                    }
                }
            }
            is ProductState.FAILURE -> {
                val message = it.failureMessage
                LaunchedEffect(key1 = message) {
                    showBanner(context, message)
                    viewModel.resetState()
                }
            }
            else -> {}
        }

    }
}

@SuppressLint("UnrememberedMutableState")
@Composable
fun UserCardView(user: Users, navController: NavController) {
    Card(
        modifier = Modifier
            .padding(8.dp, 4.dp)
            .fillMaxWidth(),
        shape = RoundedCornerShape(8.dp),
        elevation = 4.dp,
    ) {
        Row(
            Modifier
                .padding(4.dp)
                .fillMaxWidth()
                .clickable(
                    enabled = true,
                    onClick = {
                        navController.navigate(UserNavigation.AlbumView.route + "/${user.id}")
                    }
                )
        ) {
            Column(
                verticalArrangement = Arrangement.Center,
                modifier = Modifier
                    .padding(4.dp)
                    .fillMaxWidth()
                    .weight(3f)
            ) {
                Text(
                    text = user.name,
                    style = MaterialTheme.typography.subtitle1,
                    modifier = Modifier.padding(4.dp),
                    fontWeight = FontWeight.Bold,
                    maxLines = 1,
                    overflow = TextOverflow.Ellipsis
                )
                Text(
                    text = user.email,
                    modifier = Modifier.padding(4.dp),
                    maxLines = 3,
                    overflow = TextOverflow.Ellipsis
                )
            }
        }
    }
}

