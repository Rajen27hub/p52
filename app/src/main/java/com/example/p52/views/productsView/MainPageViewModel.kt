package com.example.p52.views.productsView

import android.util.Log
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.p52.api.ApiService
import com.example.p52.model.Users
import com.example.p52.room.UserDao
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Observable
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.schedulers.Schedulers
import javax.inject.Inject
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch
import javax.inject.Named

@HiltViewModel
class MainPageViewModel @Inject constructor(
    @Named("user") private val service: ApiService,
    private val userDao: UserDao
) : ViewModel() {
    val state = MutableStateFlow<ProductState>(ProductState.START)
    var errorMessage: String by mutableStateOf("")
    private val disposable = CompositeDisposable()

    private fun loadProducts() = viewModelScope.launch {
        state.value = ProductState.LOADING
        disposable.add(
            service.getUsers()
                .flatMap {
                    userDao.addUser(it)
                    Observable.just(it)
                }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    state.value = ProductState.SUCCESS(
                        userList = it
                    )
                }, { e ->
                    errorMessage = e.message.toString()
                    Log.i("ok", errorMessage)
                })
        )
    }

    fun onStart() {
        loadProducts()
    }

    fun resetState() {
        state.value = ProductState.START
    }
}

sealed class ProductState {
    object START : ProductState()
    object LOADING : ProductState()
    data class SUCCESS(
        val userList: List<Users>
    ) : ProductState()

    data class FAILURE(val failureMessage: String) : ProductState()
}