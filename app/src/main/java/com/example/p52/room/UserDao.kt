package com.example.p52.room

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.p52.model.Users


@Dao
interface UserDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun addUser(users: List<Users>)

   @Query("Select * from User")
    fun getUser(): List<Users>

}
