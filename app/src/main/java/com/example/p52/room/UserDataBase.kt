package com.example.p52.room

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.example.p52.model.AddressConverters
import com.example.p52.model.Users

@Database(entities = [Users::class], version = 1)
@TypeConverters(AddressConverters::class)

abstract class UserDataBase : RoomDatabase() {

    abstract fun getDao(): UserDao

}
