package com.example.p52.model

data class Albums(
    val userId: Int,
    val id: Int,
    val title: String
)
