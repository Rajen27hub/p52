package com.example.p52.model

import androidx.room.Entity

@Entity

class Address(
    val city: String,
)
