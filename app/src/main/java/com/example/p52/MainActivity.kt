package com.example.p52

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import com.example.p52.ui.theme.P52Theme
import com.example.p52.views.productsDetailsView.AlbumView
import com.example.p52.views.productsView.MainPageView
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            P52Theme {
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colors.background
                ) {
                    MainView()
                }
            }
        }
    }
}


@Composable
fun MainView() {
    val navController = rememberNavController()
    NavHost(
        navController = navController,
        startDestination = HOME_ROUTE,
        builder = {
            composable(route = HOME_ROUTE) {
                MainPageView(navController = navController)
            }
            composable(route = UserNavigation.AlbumView.route + "/{id}") { navBackStack ->
                val id = navBackStack.arguments?.getString("id")
                    ?: throw AssertionError(" ")
                AlbumView(id = id.toInt(), navController = navController)
            }
        })
}